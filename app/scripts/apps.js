// Initializations
import './cores/prototype';
import './initializations/import-jquery-plugins';
import './initializations/lazyload';

// Tweaks
import './tweaks/improve-window-events';
import './tweaks/add-active-state';
import './tweaks/active-element-with-enter';

// Plugins
import './plugins/template-plugin';
import './plugins/form-validation';
import './plugins/gmap';
import './plugins/recaptcha';
import './plugins/header';
import './plugins/footer';
import './plugins/scroll-to-section';
import './plugins/faq';
import './plugins/contact';
import './plugins/pricing';
import './plugins/our-blog';
import './plugins/ventures-page';
