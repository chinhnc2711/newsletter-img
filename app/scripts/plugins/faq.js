import './accordion';

@Wrapper
export default class FAQ extends Plugin {
  init () {
    let that = this,
      el = that.$element;

    el.PluginAccordion({
      colorTitle: '#464443',
      colorBtn: 'black'
    });
  }
}
