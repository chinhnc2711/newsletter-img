@Wrapper
export default class Blog extends Plugin {
  init () {
    this.pagination();
  }

  pagination() {
    let that = this,
      el = that.$element;

    let groupPagi = $(el.find('.list-pagi li'));
    groupPagi.on('click', 'button', function() {
      groupPagi.children('button').removeClass('btn-pagi-active');

      $(this).addClass('btn-pagi-active');
    });

  }
}
