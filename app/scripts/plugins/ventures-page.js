@Wrapper
export default class VenturesPage extends Plugin {
  init () {
    this.effectMenuPage();
  }

  getMenu () {
    this.menuVentures = $('html').find('.menu ul li a');// get menu
  }

  effectMenuPage () {
    let that = this,
      el = that.$element;

    this.getMenu();

    let menu = that.menuVentures;

    let dataVentures = el.data('page');

    $(menu).each( (index, item) => {
      let $item = $(item);
      let contentMenu = $item.text().toLowerCase();

      contentMenu === dataVentures ? $item.addClass('active-menu') :
        $item.removeClass('active-menu');

    });
  }
}
