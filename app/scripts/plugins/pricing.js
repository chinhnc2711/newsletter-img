import $ from 'jquery';
import Isotope from 'isotope-layout';
import jQueryBridget from 'jquery-bridget';
jQueryBridget( 'isotope', Isotope, $ );

@Wrapper
export default class Pricing extends Plugin {
  init () {
    this.isotopeButton();
    this.valueCash = 0;
    this.valueEmployees = 0;
    this.valueBankStatements = 0;

    this.createRangeSlider();
    this.submitForm();

  }

  isotopeButton() {
    let that = this,
      el = that.$element;

    let groupPrice = $(el.find('.group-price'));
    groupPrice.isotope({
      itemSelector: '.item-price',
      filter: '.getaquote'
    });

    let btnGroup = $(el.find('.group-btn-price'));
    btnGroup.on('click', 'button', function() {
      btnGroup.children().removeClass('active-price');

      let dataFilter = $(this).data('filter');
      groupPrice.isotope({filter: dataFilter});

      $(this).addClass('active-price');
    });
  }

  createRangeSlider() {
    let that = this,
      el = that.$element;

    let cashOfMonth = $(el.find('input[data-cash]'));
    let employees = $(el.find('input[data-employees]'));
    let bankStatements = $(el.find('input[data-bankstatements]'));

    cashOfMonth.ionRangeSlider({
      skin: 'big',
      onStart (data) {
        that.valueCash = data.from;
      },
      onFinish (data) {
        // Called then action is done and mouse is released
        that.valueCash = data.from;
      }
    });
    employees.ionRangeSlider({
      skin: 'big',
      onStart (data) {
        that.valueEmployees = data.from;
      },
      onFinish (data) {
        // Called then action is done and mouse is released
        that.valueEmployees = data.from;
      },
    });
    bankStatements.ionRangeSlider({
      skin: 'big',

      onStart (data) {
        that.valueBankStatements = data.from;
      },
      onFinish (data) {
        // Called then action is done and mouse is released
        that.valueBankStatements = data.from;
      }
    });
  }

  submitForm() {
    let that = this,
      el = that.$element;

    let form = $(el.find('form'));

    let btnSumit = $(el.find('button[data-submit-form-pricing]'));

    btnSumit.click(function(event) {
      event.preventDefault();

      that.valueBusiness = el.find('input[data-business]:checked').val();

      that.valueTransact = el.find('input[data-transact-in-cash]:checked').val();
      that.valueTransact === 'true' ? that.valueTransact = true : that.valueTransact = false;

      that.valuePayroll = el.find('input[data-payroll]:checked').val();
      that.valuePayroll === 'true' ? that.valuePayroll = true : that.valuePayroll = false;

      $.ajax({
        url: form.attr('action'),
        method: form.attr('method'),
        data: that.createData(),
        dataType: 'JSON',
        success() {
          //code here
        }
      });
    });

  }

  createData() {
    let that = this;

    let data = {
      valueBusiness: that.valueBusiness,
      valueCash: that.valueCash,
      valueEmployees: that.valueEmployees,
      valueBankStatements: that.valueBankStatements,
      valueTransact: that.valueTransact,
      valuePayroll: that.valuePayroll
    };
    return data;
  }
}
