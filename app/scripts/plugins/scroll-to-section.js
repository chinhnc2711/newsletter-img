@Wrapper
export default class ScrollToSection extends Plugin {
  init () {
    let that = this,
      el = that.$element;

    let fullSection = $(el.find('section'));
    let pointSection = $(el.find('.point'));

    that.groupSection = [];
    for (let item of fullSection) {
      for ( let itemPoint of pointSection ) {
        if ($(item).attr('id') === $(itemPoint).attr('name')) {
          that.groupSection = [
            ...that.groupSection, { section: item, point: itemPoint }
          ];
        }
      }
    }

    this.clickPointScrollSection();
    this.scrollSection();
  }

  clickPointScrollSection() {
    let that = this;

    $(that.groupSection).each((index, item) => {

      let { section, point } = item;

      $(point).click( function() {
        $('html, body').stop(true, true);

        let poSe = $(section).position().top - 100;

        $('html, body').animate({scrollTop: poSe}, 700);

      });
    });
  }

  scrollSection () {
    let that = this;
    let tagSection = function() {
      let coorScrollBar = $('html').scrollTop();

      $(that.groupSection).each((index, item) => {
        let { section, point } = item;
        $(point).removeClass('active');

        let heightSection = $(section).offset().top + $(section).height();

        if (coorScrollBar > $(section).offset().top - 200 &&
                            coorScrollBar < heightSection){
          $(point).addClass('active');
        }
      });
    };

    $(window).scroll(function() {
      tagSection();
    });

    tagSection();
  }
}
