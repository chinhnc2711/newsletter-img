import Handlebars from 'handlebars';

(function($) {
  $.fn.PluginAccordion = function(params) {
    let defaults = {
      colorTitle: '#ff8a00',
      colorContent: 'black',
      colorBorder: '#cfcfcf',
      colorBtn: 'linear-gradient(#ffcc00, #ff8a00)',
      backgroundTitle: '#f5f5f5',
      backgroundContent: '#fff',
      animate: 400,
      addClassAccor: ''
    };

    let options = $.extend({}, defaults, params);
    let { colorContent, colorBtn,
      backgroundContent, animate } = options;


    let teamplateWrapper = Handlebars.compile('<div class={{class}}></div>'),
      templateQuestion = Handlebars.compile(
        '<div class="{{classQuestion}}""></div>'
      ),
      teamplateAnswer = Handlebars.compile(
        '<div class={{classAnswer}}></div>'
      ),
      teamplateButton = Handlebars.compile(
        '<button type="button" class={{classBtn}}><div class="line"></div><div class="line"></div></button>'
      );

    this.append(teamplateWrapper({class: 'accor'}));
    this.find('.accor').append('<ul class="list-accor"></ul>');

    let h3 = [...this.find('h3')],
      p = [...this.find('p')];

    // Create Item
    for (let item of h3) {
      let templateLi = Handlebars.compile(
        '<li class="item"></li>'
      );
      this.find('ul').append(templateLi);
    }

    // Append Question and Answer
    let itemLi = this.find('.item');
    for (let item of itemLi) {
      $(item).append(templateQuestion({classQuestion: 'question'}))
            .append(teamplateAnswer({classAnswer: 'answer'}));
    }

    let question = [...this.find('.question')];
    let answer = [...this.find('.answer')];

    for (let [index, item] of question.entries()) {
      $(item)
        .append(h3[index])
        .append(teamplateButton({classBtn: 'hide-answer'}));
    }

    for (let [index, item] of answer.entries()) {
      $(item)
        .append(p[index]);
    }

    this.find('h3').addClass('question-content');
    this.find('p').addClass('question-answer');

    let $itemAccordion = $(this.find('.item')),
      btn = this.find('button');

    $(btn).children().css({'background': colorBtn});
    $(answer).css({'background': backgroundContent});
    $(answer).children().css({'color': colorContent});

    $itemAccordion.each(function(index, item) {
      let $question = $(item).find('.question');

      $question.click(function() {
        let $this = $(this),
          answerFocus = $this.next(),
          btnFocus = $this.find('button');

        let method = (answerFocus.css('display') === 'block' ? true : false);

        if (method) {
          answerFocus.slideUp(animate);
          btnFocus.removeClass('show-answer');
        } else {
          $(answer).slideUp(animate);
          answerFocus.slideDown(animate);

          btn.removeClass('show-answer');
          btnFocus.addClass('show-answer');
        }
      });
    });
  };
  return this;
})(jQuery);

// Plugin Accordion ( Cong Phat )
export default this;
