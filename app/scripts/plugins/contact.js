import toastr from 'toastr';
toastr.options = {
  'positionClass': 'toast-bottom-right',
};

@Wrapper({
  options: {
    errorsWrapper: '<div class="message-email"></div>',
    errorTemplate: '<span></span>'
  }
})
export default class Contact extends Plugin {
  init () {
    let that = this,
      el = that.$element;

    let formContact = $(el.find('form'));
    formContact.parsley(this.options);

    formContact.submit(function(event) {
      event.preventDefault();

      let $this = $(this);

      //Get data input
      let valueName = $($this.find('input[name="name"]')).val(),
        valueSubject = $($this.find('input[name="subject"]')).val(),
        valueEmail = $($this.find('input[name="email"]')).val(),
        valuePhoneNumber = $($this.find('input[name="phone"]')).val(),
        valueMessage = $($this.find('textarea[name="message"]')).val();

      let data = {
        valueName,
        valueSubject,
        valueEmail,
        valuePhoneNumber,
        valueMessage
      };

      $.ajax({
        url: $this.attr('action'),
        method: $this.attr('method'),
        data,
        // dataType: 'JSON',
        // beforeSend() {

        // },
        success() {
          $this.find('input').val('');

          $this.parsley().reset();

          toastr.success('Success');
        }
      });
    });
  }
}
