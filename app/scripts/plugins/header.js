@Wrapper
export default class Header extends Plugin {
  init () {
    let that = this,
      el = that.$element;

    let $hamburger = $(el.find('.hamburger'));
    let $nav = $(el.find('.menu'));

    $hamburger.click( function() {
      $nav.stop();

      if ($nav.css('display') === 'none' ) {
        $(this).children().addClass('ef-op');
      } else {
        $(this).children().removeClass('ef-op');
      }

      $nav.toggle('fade-in');

    });

    this.btnSearch = el.find('.btn-search');
    let widthWindow = $(window).width();
    widthWindow < 576 ? this.focusSearchMobile() : this.toggleSearchDesktop();
  }

  focusSearchMobile() {
    let that = this,
      el = that.$element;

    let $searchMobile = $(el.find('input[data-search-mobile]'));
    $(this.btnSearch).click(function() {
      $searchMobile.focus();
    });
  }

  toggleSearchDesktop() {
    let that = this,
      el = that.$element;

    let $iSearchDesktop = $(el.find('.input-search'));
    $(this.btnSearch).click(function() {
      let src;
      $iSearchDesktop.css('display') === 'none' ?
        src = './../images/icon-close.png' :
        src = './../images/icon-search.png';

      $(this).find('img').attr('src', src);

      $iSearchDesktop.stop().toggle().children().focus();
    });
  }
}
